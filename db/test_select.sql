select os.jmeno||' '||os.prijmeni||' ('||ofce.nazev||')' jmeno, sp.nazev||' ('||sp.ico::varchar(8)||')' spolecnost,
	st.nazev typ_sidla, adru.nazev||' '||adrc.domovni||'/'||adrc.orientacni||', '||adro.nazev||', '||adrs.nazev adresa
from osoba os
	inner join osoba_funkce ofce on os.osoba_funkce = ofce.id
	inner join spolecnost sp on os.spolecnost = sp.id
	inner join sidlo si on os.sidlo = si.id
	inner join spolecnost_sidlo ss on (ss.id_spolecnost = os.spolecnost and ss.id_sidlo = os.sidlo)
	inner join sidlo_typ st on ss.sidlo_typ = st.id
	inner join adr_stat adrs on adrs.id = si.adr_stat
	inner join adr_obec adro on adro.id = si.adr_obec
	inner join adr_ulice adru on adru.id = si.adr_ulice
	inner join adr_cislo adrc on adrc.id = si.adr_cislo;