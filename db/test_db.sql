--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: test; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Czech_Czechia.1250';


ALTER DATABASE test OWNER TO postgres;

\connect test

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: data; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA data;


ALTER SCHEMA data OWNER TO postgres;

--
-- Name: SCHEMA data; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA data IS 'data';


--
-- Name: trgfc_nova_spolecnost(); Type: FUNCTION; Schema: data; Owner: postgres
--

CREATE FUNCTION data.trgfc_nova_spolecnost() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare 
	id_sidloX int;
	id_spolecnost_sidloX int;
	id_osobaX int;
begin
	select max(id)+1 into id_sidloX from data.sidlo;
	insert into data.sidlo (id, nazev, adr_stat, adr_obec, adr_ulice, adr_cislo) values
		(id_sidloX, 'Nové centrum světa', 1, 1, 1, 1);
	
	select max(id)+1 into id_spolecnost_sidloX from data.spolecnost_sidlo ;
	insert into data.spolecnost_sidlo (id, id_spolecnost, id_sidlo, sidlo_typ) values
		(id_spolecnost_sidloX, new.id, id_sidloX, 1);
	
	select max(id)+1 into id_osobaX from data.osoba;
	insert into data.osoba(id, jmeno, prijmeni, osoba_funkce, spolecnost, sidlo) values
		(id_osobaX, 'Petr', 'Nový', 1, new.id, id_sidloX);
	
	return new;
end;
$$;


ALTER FUNCTION data.trgfc_nova_spolecnost() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: adr_cislo; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.adr_cislo (
    id integer NOT NULL,
    orientacni character varying(5),
    domovni numeric(10,0),
    psc numeric(5,0),
    adr_ulice integer NOT NULL
);


ALTER TABLE data.adr_cislo OWNER TO postgres;

--
-- Name: adr_obec; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.adr_obec (
    id integer NOT NULL,
    nazev character varying(255),
    adr_stat integer NOT NULL
);


ALTER TABLE data.adr_obec OWNER TO postgres;

--
-- Name: adr_stat; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.adr_stat (
    id integer NOT NULL,
    nazev character varying(255)
);


ALTER TABLE data.adr_stat OWNER TO postgres;

--
-- Name: adr_ulice; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.adr_ulice (
    id integer NOT NULL,
    nazev character varying(255),
    adr_obec integer NOT NULL
);


ALTER TABLE data.adr_ulice OWNER TO postgres;

--
-- Name: osoba; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.osoba (
    id integer NOT NULL,
    jmeno character varying(50),
    prijmeni character varying(50),
    osoba_funkce integer,
    spolecnost integer,
    sidlo integer
);


ALTER TABLE data.osoba OWNER TO postgres;

--
-- Name: osoba_funkce; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.osoba_funkce (
    id integer NOT NULL,
    nazev character varying(50)
);


ALTER TABLE data.osoba_funkce OWNER TO postgres;

--
-- Name: sidlo; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.sidlo (
    id integer NOT NULL,
    nazev character varying(255),
    adr_stat integer,
    adr_obec integer,
    adr_ulice integer,
    adr_cislo integer
);


ALTER TABLE data.sidlo OWNER TO postgres;

--
-- Name: sidlo_typ; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.sidlo_typ (
    id integer NOT NULL,
    nazev character varying(30)
);


ALTER TABLE data.sidlo_typ OWNER TO postgres;

--
-- Name: spolecnost; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.spolecnost (
    id integer NOT NULL,
    nazev character varying(255),
    ico numeric(8,0) NOT NULL
);


ALTER TABLE data.spolecnost OWNER TO postgres;

--
-- Name: spolecnost_sidlo; Type: TABLE; Schema: data; Owner: postgres
--

CREATE TABLE data.spolecnost_sidlo (
    id integer NOT NULL,
    id_spolecnost integer NOT NULL,
    id_sidlo integer NOT NULL,
    sidlo_typ integer NOT NULL
);


ALTER TABLE data.spolecnost_sidlo OWNER TO postgres;

--
-- Data for Name: adr_cislo; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.adr_cislo (id, orientacni, domovni, psc, adr_ulice) FROM stdin;
1	1a	102	62100	1
2	1b	103	62100	1
3	2	104	62102	2
4	3	105	62102	2
5	4b	106	62104	3
6	5c	107	62104	3
7	6	108	62104	3
8	8	109	62107	4
9	9	110	62108	5
10	10	111	62108	5
11	1a	112	62108	5
12	1b	113	62111	6
13	2	114	62111	6
14	3	115	62113	7
15	4b	116	62113	7
16	5c	117	62113	7
17	6	118	62116	8
18	8	119	62116	8
19	9	120	62118	9
20	10	121	62118	9
21	1a	122	62118	9
22	1b	123	62121	10
23	2	124	62121	10
24	3	125	62121	10
25	4b	126	62124	11
26	5c	127	62124	11
27	6	128	62126	12
28	8	129	62126	12
29	9	130	62126	12
30	10	131	62129	13
\.


--
-- Data for Name: adr_obec; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.adr_obec (id, nazev, adr_stat) FROM stdin;
1	Brno	1
2	Praha	1
3	Ostrava	1
4	Bratislava	2
5	Košice	2
\.


--
-- Data for Name: adr_stat; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.adr_stat (id, nazev) FROM stdin;
1	Česká republika
2	Slovensko
\.


--
-- Data for Name: adr_ulice; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.adr_ulice (id, nazev, adr_obec) FROM stdin;
1	Žilkova	1
2	nám. Svobody	1
3	Palackého	1
4	Mostecká	2
5	Hradecká	2
6	Budějovická	2
7	Dělnická	3
8	Nerudova	3
9	Hornická	3
10	Národní třída	4
11	nám. SNP	4
12	Štefánikova	5
13	Jánošíkova	5
\.


--
-- Data for Name: osoba; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.osoba (id, jmeno, prijmeni, osoba_funkce, spolecnost, sidlo) FROM stdin;
1	Franta	Škoda	1	1	2
2	Pepa	VW	2	1	2
3	Honza	Audi	3	1	1
4	Tonda	Mercedes	4	1	4
5	Lojza	Toyota	5	1	1
6	Karel	Triumph	1	2	1
7	Michal	Honda	2	2	3
8	Svatopluk	Suzuki	3	2	3
9	David	Yamaha	4	2	2
10	Martin	Kawasaki	5	2	2
11	Janko	Author	1	3	5
12	Marian	Merida	2	3	5
13	Martin	Liberta	3	3	3
14	Boris	Favorit	4	3	5
15	Peter	Laurin & Klement	5	3	5
16	Petr	Nový	1	4	6
\.


--
-- Data for Name: osoba_funkce; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.osoba_funkce (id, nazev) FROM stdin;
1	jednatel
2	ředitel
3	technický pracovník
4	ekonom
5	recepční
\.


--
-- Data for Name: sidlo; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.sidlo (id, nazev, adr_stat, adr_obec, adr_ulice, adr_cislo) FROM stdin;
1	Centrum světa	1	1	1	2
2	M-Mrakodrap	1	2	5	10
3	Podzemní bunkr	1	3	7	15
4	Přístav	2	4	11	26
5	Kůlna	2	5	13	30
6	Nove centrum světa	1	1	1	1
\.


--
-- Data for Name: sidlo_typ; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.sidlo_typ (id, nazev) FROM stdin;
1	ředitelství
2	pobočka
3	zahraniční pobočka
\.


--
-- Data for Name: spolecnost; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.spolecnost (id, nazev, ico) FROM stdin;
1	Auto, s.r.o.	12345678
2	Motorka, a.s.	87654321
3	Kolo, s.r.o.	11122333
4	Nová společnost	45678123
\.


--
-- Data for Name: spolecnost_sidlo; Type: TABLE DATA; Schema: data; Owner: postgres
--

COPY data.spolecnost_sidlo (id, id_spolecnost, id_sidlo, sidlo_typ) FROM stdin;
1	1	2	1
2	1	1	2
3	1	4	3
4	2	1	1
5	2	3	2
6	2	2	2
7	3	5	1
8	3	3	3
9	4	6	1
\.


--
-- Name: adr_cislo adr_cislo_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_cislo
    ADD CONSTRAINT adr_cislo_pkey PRIMARY KEY (id);


--
-- Name: adr_obec adr_obec_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_obec
    ADD CONSTRAINT adr_obec_pkey PRIMARY KEY (id);


--
-- Name: adr_stat adr_stat_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_stat
    ADD CONSTRAINT adr_stat_pkey PRIMARY KEY (id);


--
-- Name: adr_ulice adr_ulice_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_ulice
    ADD CONSTRAINT adr_ulice_pkey PRIMARY KEY (id);


--
-- Name: osoba_funkce osoba_funkce_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.osoba_funkce
    ADD CONSTRAINT osoba_funkce_pkey PRIMARY KEY (id);


--
-- Name: osoba osoba_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.osoba
    ADD CONSTRAINT osoba_pkey PRIMARY KEY (id);


--
-- Name: sidlo sidlo_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo
    ADD CONSTRAINT sidlo_pkey PRIMARY KEY (id);


--
-- Name: sidlo_typ sidlo_typ_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo_typ
    ADD CONSTRAINT sidlo_typ_pkey PRIMARY KEY (id);


--
-- Name: spolecnost spolecnost_ico_key; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost
    ADD CONSTRAINT spolecnost_ico_key UNIQUE (ico);


--
-- Name: spolecnost spolecnost_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost
    ADD CONSTRAINT spolecnost_pkey PRIMARY KEY (id);


--
-- Name: spolecnost_sidlo spolecnost_sidlo_pkey; Type: CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost_sidlo
    ADD CONSTRAINT spolecnost_sidlo_pkey PRIMARY KEY (id);


--
-- Name: spolecnost trg_nova_spolecnost; Type: TRIGGER; Schema: data; Owner: postgres
--

CREATE TRIGGER trg_nova_spolecnost AFTER INSERT ON data.spolecnost FOR EACH ROW EXECUTE FUNCTION data.trgfc_nova_spolecnost();


--
-- Name: sidlo fk_adr_cislo; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo
    ADD CONSTRAINT fk_adr_cislo FOREIGN KEY (adr_cislo) REFERENCES data.adr_cislo(id);


--
-- Name: adr_ulice fk_adr_obec; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_ulice
    ADD CONSTRAINT fk_adr_obec FOREIGN KEY (adr_obec) REFERENCES data.adr_obec(id);


--
-- Name: sidlo fk_adr_obec; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo
    ADD CONSTRAINT fk_adr_obec FOREIGN KEY (adr_obec) REFERENCES data.adr_obec(id);


--
-- Name: adr_obec fk_adr_stat; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_obec
    ADD CONSTRAINT fk_adr_stat FOREIGN KEY (adr_stat) REFERENCES data.adr_stat(id);


--
-- Name: sidlo fk_adr_stat; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo
    ADD CONSTRAINT fk_adr_stat FOREIGN KEY (adr_stat) REFERENCES data.adr_stat(id);


--
-- Name: adr_cislo fk_adr_ulice; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.adr_cislo
    ADD CONSTRAINT fk_adr_ulice FOREIGN KEY (adr_ulice) REFERENCES data.adr_ulice(id);


--
-- Name: sidlo fk_adr_ulice; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.sidlo
    ADD CONSTRAINT fk_adr_ulice FOREIGN KEY (adr_ulice) REFERENCES data.adr_ulice(id);


--
-- Name: osoba fk_osoba_funkce; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.osoba
    ADD CONSTRAINT fk_osoba_funkce FOREIGN KEY (osoba_funkce) REFERENCES data.osoba_funkce(id);


--
-- Name: spolecnost_sidlo fk_sidlo; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost_sidlo
    ADD CONSTRAINT fk_sidlo FOREIGN KEY (id_sidlo) REFERENCES data.sidlo(id);


--
-- Name: osoba fk_sidlo; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.osoba
    ADD CONSTRAINT fk_sidlo FOREIGN KEY (sidlo) REFERENCES data.sidlo(id);


--
-- Name: spolecnost_sidlo fk_sidlo_typ; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost_sidlo
    ADD CONSTRAINT fk_sidlo_typ FOREIGN KEY (sidlo_typ) REFERENCES data.sidlo_typ(id);


--
-- Name: spolecnost_sidlo fk_spolecnost; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.spolecnost_sidlo
    ADD CONSTRAINT fk_spolecnost FOREIGN KEY (id_spolecnost) REFERENCES data.spolecnost(id);


--
-- Name: osoba fk_spolecnost; Type: FK CONSTRAINT; Schema: data; Owner: postgres
--

ALTER TABLE ONLY data.osoba
    ADD CONSTRAINT fk_spolecnost FOREIGN KEY (spolecnost) REFERENCES data.spolecnost(id);


--
-- PostgreSQL database dump complete
--

