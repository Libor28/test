# -*- encoding: utf-8 -*-
import smtplib, ssl, configparser
import pandas as pd
import os
import logging

from numpy import NaN #pro nacitani soubor .ini
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s;%(asctime)s;%(name)s;%(message)s')
file_handler = logging.FileHandler('test.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

class DataProcessing:
    def __init__(self, file_loc, *args): # v do args se pisou nazvy sloupcu pro predelani desetinne carky na tecku
        logger.info('initiliazitaion of file ' + file_loc + ' arguments: ' + str(args))

        self.file_loc = file_loc
        self.file_name = '.'.join(os.path.basename(file_loc).split('.')[:-1])

        self.converters = {} # pro konverzi desetinne carky na tecku
        for arg in args:    # format pak vypada nasledovne {'Délka': decimal_converter, 'Plocha VB (TAB)': decimal_converter, 'Plocha VB (VFK GP)': decimal_converter}
            self.converters[arg] = self.decimal_converter
            
        self.df = pd.read_excel(file_loc, converters=self.converters)
        self.config = configparser.ConfigParser()
        self.config.read('test.ini')

    def decimal_converter(self, value): # konvertuje desetinnou carku na tecku
        logger.info('converting decimal comma to point')
        try:
            return float(value.replace(',', '.'))
        except ValueError:
            logger.info('value [' + value + "] can't change decimal comma to point")
            return value

    def num_of_cols(self): # vrati pocet sloupcu
        logger.info('computing number of columns')
        return (len(self.df.columns))

    def num_of_rows(self): # vrati pocet radku
        logger.info('computing number of rows')
        return (len(self.df.index))

    def unique_vals_col(self, column): # vrati pocet unikatnich hodnot ve sloupci
        logger.info('computing unique values in columns')
        return (self.df[column].nunique())

    def null_vals_col(self, column): # vrati pocet prazdnych hodnot ve sloupci
        logger.info('computing empty values in columns')
        return (self.df[column].isnull().sum())

    def min_val_col(self, column): # vrati minimalni hodnotu ve sloupci
        logger.info('computing minimal value in column [' + column + ']')
        min_val = self.df[column].dropna().min(skipna=True)
        try:
            min_val = pd.to_numeric(min_val)
            return min_val
        except ValueError:
            logger.info('value [' + min_val + "] can't get minimal value")
            return False
    
    def max_val_col(self, column): # vrati maximalni hodnotu ve sloupci
        logger.info('computing maximal value in column [' + column + ']')
        max_val = self.df[column].dropna().max()
        try:
            max_val = pd.to_numeric(max_val)
            return max_val
        except ValueError:
            logger.info('value [' + max_val + "] can't get maximal value")
            return False

    def mean_val_col(self, column): # vrati prumernou honotu ve sloupci
        logger.info('computing mean value in column [' + column + ']')
        for index, row in self.df.iterrows():
            if (isinstance(row[column], str)):
                logger.info('column [' + column + "] has string value, can't get mean value")
                return False
            else:
                try:
                    mean_val = self.df[column].dropna().mean()
                    mean_val = pd.to_numeric(mean_val)
                    return mean_val
                except ValueError:
                    logger.info('value [' + mean_val + "] can't get mean value")
                    return False

    def save_to_csv(self): # ulozi do csv
        logger.info('saving to CSV')
        self.df.to_csv(self.file_name+'.csv', encoding='utf-8-sig', index=False)

    def make_statistics(self): # spocita vsechny statistiky
        logger.info('making statistics')
        with open(self.file_name+'_statistika.log', 'w') as f:
            f.write('OBECNÉ\nnázev souboru: '+ self.file_name + '\npočet sloupců: ' + str(self.num_of_cols()) + '\npočet řádků: ' + str(self.num_of_rows())+'\n\nSTATISTIKY PRO SLOUPCE\n')
            for col in self.df.columns:
                f.write(col 
                + '\n počet unikátních hodnot: ' + str(self.unique_vals_col(col)) 
                + '\n počet prázdných hodnot: ' + str(self.null_vals_col(col))
                + '\n minimální hodnota: ' + str(self.min_val_col(col))
                + '\n maximální hodnota: ' + str(self.max_val_col(col))
                + '\n průměrná hodnota: ' + str(self.mean_val_col(col)) + '\n\n'
                )  

    def send_mail(self): # posle mail s vypocitanymi vysledky na adresu z test.ini
        logger.info('sending email')
        sender_email = self.config['EMAIL']['sender_email']
        password = self.config['EMAIL']['password']
        receiver_email = self.config['EMAIL']['receiver_email']
        subject = 'Test'
        body = 'Toto je mail s výsledkem testu'
        smtp = self.config['EMAIL']['smtp']
        port = self.config['EMAIL']['port']
        
        message = MIMEMultipart()
        message['From'] = sender_email
        message['To'] = receiver_email
        message['Subject'] = subject
        message.attach(MIMEText(body, 'plain'))

        filename = self.file_name+'_statistika.log'
        try:
            with open(filename, "rb") as attachment:
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.read())
        except FileNotFoundError as e:
            logger.exception('file [' + e.filename + '] does not exist')
            return False
            
        encoders.encode_base64(part)
        part.add_header(
            "Content-Disposition",
            f"attachment; filename= {filename}",
        )
        message.attach(part)

        text = message.as_string()

        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp, port, context=context) as server:
            try:
                server.login(sender_email, password)
            except:
                logger.exception('cannot log in to SMTP server')
            else:
                try:
                    server.sendmail(sender_email, receiver_email, text)
                except:
                    logger.exception('email sending failed')

            


excel = DataProcessing('223344.xlsx', 'Délka', 'Plocha VB (TAB)', 'Plocha VB (VFK GP)')
excel.save_to_csv()
excel.make_statistics()
excel.send_mail()
