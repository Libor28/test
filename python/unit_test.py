import pandas as pd
import unittest
from test import DataProcessing

df = pd.DataFrame({
        'sloupec1': ['X','','X','X'],
        'sloupec2': ['1,2','1,8','','3']
    })
writer = pd.ExcelWriter('unit_test.xlsx', engine='xlsxwriter')
df.to_excel(writer, sheet_name='unit_test', index=False)
writer.save()


class TestNumCols(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_num_of_cols(self):
        result = self.excel.num_of_cols()
        print (result)
        self.assertEqual(result, 2)

class TestNumRows(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_num_of_rows(self):
        result = self.excel.num_of_rows()
        print (result)
        self.assertEqual(result, 4)

class TestUniqueVals(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_unique_vals(self):
        result = self.excel.unique_vals_col('sloupec2')
        print (result)
        self.assertEqual(result, 3)

class TestNullVals(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_null_vals(self):
        result = self.excel.null_vals_col('sloupec1')
        print (result)
        self.assertEqual(result, 1)

class TestMaxVal(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_unique_vals(self):
        result = self.excel.max_val_col('sloupec2')
        print (result)
        self.assertEqual(result, 3)

class TestMinVal(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_unique_vals(self):
        result = self.excel.min_val_col('sloupec2')
        print (result)
        self.assertEqual(result, 1.2)

class TestMeanVal(unittest.TestCase):
    def setUp(self):
        self.excel = DataProcessing('unit_test.xlsx', 'sloupec2')

    def test_unique_vals(self):
        result = self.excel.mean_val_col('sloupec2')
        print (result)
        self.assertEqual(result, 2)




if __name__ == '__main__':
    unittest.main()

